use std::f32::consts::PI;
use bevy::prelude::*;
use rand::Rng;

const PARTICLE_COUNT: u16 = 25;
const PARTICLE_DAMPING: f32 = 0.8;
const PARTICLE_THICKNESS: f32 = 0.25;

// Type Definition: --------------------------------------------------------------------------------

pub struct EffectsPlugin;

#[derive(Component)]
pub struct GrowIn {
	pub target_size: Vec2,
	pub speed: f32
}

#[derive(Resource)]
pub struct Particles{
	particles: Vec<ParticleData>,
	current_particle_id: u16
}

struct ParticleData{
	color: Color,
	position: Vec2,
	velocity: Vec2
}

#[derive(Component)]
struct Particle{
	id: u16
}

// Type Implementations: ---------------------------------------------------------------------------

impl Plugin for EffectsPlugin {
	fn build(&self, app: &mut App) {
		app
			.insert_resource(Particles{
				particles: Vec::new(),
				current_particle_id: 0
			})
			.add_systems(Startup, initialize)
			.add_systems(FixedUpdate, handle_particle_damping)
			.add_systems(Update, handle_particles)
			.add_systems(Update, start_grow_in.before(handle_grow_in))
			.add_systems(Update, handle_grow_in)
		;
	}
}

impl Particles {
	fn get_next_data(&mut self) -> &mut ParticleData {
		self.current_particle_id = (self.current_particle_id + 1) % self.particles.len() as u16;
		return &mut self.particles[self.current_particle_id as usize];
	}
}

// Systems: ----------------------------------------------------------------------------------------

fn initialize(mut commands: Commands, mut particles: ResMut<Particles>){
	for i in 0 .. PARTICLE_COUNT {
		commands.spawn((
			SpriteBundle{
				transform: Transform {
					translation: Vec3::new(0.0, 0.0, 0.5),
					scale: Vec3::new(0.0, 0.0, 1.0),
					..Default::default()
				},
				..Default::default()
			},
			Particle {
				id: i
			}
		));
		particles.particles.push(ParticleData { 
			color: Color::WHITE,
			position: Vec2::ZERO, 
			velocity: Vec2::ZERO 
		});
	}
}

fn handle_particles(
	time: Res<Time>, 
	mut particles: ResMut<Particles>, 
	mut query: Query<(&mut Transform, &mut Sprite, &Particle)>
) {
	let dt = time.delta_seconds();
	for (mut transform, mut sprite, particle) in &mut query {
		let particle_data: &mut ParticleData = &mut particles.particles[particle.id as usize];

		sprite.color = particle_data.color;
		particle_data.position += particle_data.velocity * dt;
		transform.translation.x = particle_data.position.x;
		transform.translation.y = particle_data.position.y;

		let size = particle_data.velocity.length() * 0.1;
		transform.scale.x = size.min(PARTICLE_THICKNESS);
		transform.scale.y = size;

		transform.rotation = Quat::from_euler(
			EulerRot::ZXY, 
			particle_data.velocity.y.atan2(particle_data.velocity.x), 
			0.0, 0.0
		);
	}
}

fn handle_particle_damping(mut particles: ResMut<Particles>){
	for particle in &mut particles.particles {
		particle.velocity *= PARTICLE_DAMPING;
	}
}

fn start_grow_in(mut query: Query<&mut Transform, Added<GrowIn>>){
	for mut transform in &mut query {
		transform.scale = Vec3::new(0.0, 0.0, 1.0);
	}
}

fn handle_grow_in(
	mut commands: Commands, 
	time: Res<Time>, 
	mut query: Query<(Entity, &mut Transform, &GrowIn)>
) {
	let dt = time.delta_seconds();
	for (entity, mut transform, grow_in) in &mut query {
		let grow_delta = grow_in.target_size * dt * grow_in.speed;
		transform.scale += Vec3::new(grow_delta.x, grow_delta.y, 0.0);

		// if target size met, remove grow in component and set final size
		if transform.scale.x >= grow_in.target_size.x {
			transform.scale.x = grow_in.target_size.x;
			transform.scale.y = grow_in.target_size.y;
			commands.entity(entity).remove::<GrowIn>();
		}
	}
}

// Utility: ----------------------------------------------------------------------------------------

pub fn particle_burst(
	particles: &mut ResMut<Particles>, 
	color: Color,
	position: Vec2, 
	count: u16, 
	speed: f32
) {
	for i in 0 .. count {
		let particle_data = particles.get_next_data();
		
		// set particle color
		particle_data.color = color;

		// set position
		particle_data.position.x = position.x;
		particle_data.position.y = position.y;

		// set velocity to speed at random angle
		let ang_variance = 2.0 * PI / count as f32;
		let ang: f32 = rand::thread_rng().gen_range(0.0 .. ang_variance) + i as f32 * ang_variance;
		let vel = Vec2::from_angle(ang) * speed;
		particle_data.velocity.x = vel.x;
		particle_data.velocity.y = vel.y;
	}
}

pub fn particle_half_burst(
	particles: &mut ResMut<Particles>, 
	color: Color,
	position: Vec2, 
	count: u16, 
	speed: f32,
	angle: f32
) {
	for i in 0 .. count {
		let particle_data = particles.get_next_data();
		
		// set particle color
		particle_data.color = color;

		// set position
		particle_data.position.x = position.x;
		particle_data.position.y = position.y;

		// set velocity to speed at random angle
		let ang_variance = PI / count as f32;
		let ang: f32 = rand::thread_rng().gen_range(0.0 .. ang_variance) + i as f32 * ang_variance + angle - PI * 0.5;
		let vel = Vec2::from_angle(ang) * rand::thread_rng().gen_range(speed * 0.75 .. speed);
		particle_data.velocity.x = vel.x;
		particle_data.velocity.y = vel.y;
	}
}

pub fn particle_narrow_burst(
	particles: &mut ResMut<Particles>, 
	color: Color,
	position: Vec2, 
	count: u16, 
	speed: f32,
	angle: f32
){
	for i in 0 .. count {
		let particle_data = particles.get_next_data();
		
		// set particle color
		particle_data.color = color;

		// set position
		particle_data.position.x = position.x;
		particle_data.position.y = position.y;

		// set velocity to speed at random angle
		let ang_variance = PI * 0.35 / count as f32;
		let ang: f32 = rand::thread_rng().gen_range(0.0 .. ang_variance) + i as f32 * ang_variance + angle - PI * 0.175;
		let vel = Vec2::from_angle(ang) * rand::thread_rng().gen_range(speed * 0.75 .. speed);
		particle_data.velocity.x = vel.x;
		particle_data.velocity.y = vel.y;
	}
}