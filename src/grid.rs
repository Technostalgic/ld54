use std::ops::{
	Add, 
	AddAssign,
	Sub
};
use rand::*;
use bevy::{
	prelude::*, 
	utils::HashMap
};
use crate::crawler;
use crate::effects;

const MAX_PELLETS: u8 = 2;
const ROOM_SIZE: i16 = 7;

// Type Defintions: --------------------------------------------------------------------------------

pub struct GridPlugin;

// a coordinate that points to a location in the grid
#[derive(Clone, Copy)]
pub struct Coordinate {
	pub x: i16,
	pub y: i16
}

// grid object is used to store world data
#[derive(Resource)]
pub struct Grid{
	data: HashMap<(i16, i16), Cell>,
	modified: Vec<(i16, i16)>
}

// used to keep track of spawned pellets
#[derive(Resource)]
pub struct PelletSpawner{
	pub pellet_count: u8
}

#[derive(Component)]
pub struct GridVisualizer {
	entities: HashMap<(i16, i16), Entity>
}

// cells used to describe data within the grid
#[derive(PartialEq, Clone, Copy)]
pub enum Cell {
	Wall,
	Pellet,
	None
}

// Type Implementations: ---------------------------------------------------------------------------

// initialize plugin for grid
impl Plugin for GridPlugin {
	fn build(&self, app: &mut App) {
		app
			.insert_resource(
				Grid {
					data: HashMap::new(),
					modified: Vec::new()
				}
			)
			.insert_resource( 
				PelletSpawner { pellet_count: 0 } 
			)
			.add_systems(Startup, initialize)
			.add_systems(Update, handle_grid_visuals.before(crawler::handle_crawler_movement))
			.add_systems(Update, handle_pellet_spawning)
		;
	}
}

impl From<Coordinate> for Vec3 {
	fn from(value: Coordinate) -> Self {
		Vec3{ x: value.x as f32, y: value.y as f32, z: 0.0 }
	}
}

impl From<Coordinate> for Vec2 {
	fn from(value: Coordinate) -> Self {
		Vec2{ x: value.x as f32, y: value.y as f32 }
	}
}

// implement cast from coordinate to tuple
impl From<Coordinate> for (i16, i16) {
	fn from(value: Coordinate) -> Self {
		(value.x, value.y)
	}
}

// implement cast from tuple to coordinate
impl From<(i16, i16)> for Coordinate {
	fn from(value: (i16, i16)) -> Self {
		Coordinate { x: value.0, y: value.1 }
	}
}

impl From<&(i16, i16)> for Coordinate {
	fn from(value: &(i16, i16)) -> Self {
		Coordinate { x: value.0, y: value.1 }
	}
}

// implement addition operator for coordinates
impl Add for Coordinate {
	type Output = Coordinate;
	fn add(self, rhs: Self) -> Self::Output {
		Coordinate {
			x: self.x + rhs.x,
			y: self.y + rhs.y
		}
	}
}

impl AddAssign for Coordinate {
	fn add_assign(&mut self, rhs: Self) {
		self.x += rhs.x;
		self.y += rhs.y;
	}
}

// implement subtraction operator for coordinates
impl Sub for Coordinate {
	type Output = Coordinate;
	fn sub(self, rhs: Self) -> Self::Output {
		Coordinate {
			x: self.x - rhs.x,
			y: self.y - rhs.y
		}
	}
}

impl Cell {
	pub fn is_solid(&self) -> bool {
		match self {
			Cell::None => false,
			Cell::Pellet => false,
			_ => true
		}
	}
}

// implement getter and setter for grid data
impl Grid {
	pub fn get_cell_at(&self, coordinate: Coordinate) -> Cell {
		let tuple: (i16, i16) = coordinate.into();
		let val = self.data.get(&tuple);
		if let Some(val) = val {
			return *val;
		}
		Cell::None
	}
	pub fn set_cell_at(&mut self, coordinate: Coordinate, value: Cell) {
		self.data.insert(coordinate.into(), value);
		self.modified.push(coordinate.into());
	}
}

impl GridVisualizer {
	fn add_cell(&mut self, commands: &mut Commands, coordinate: (i16, i16), cell: Cell){
		if cell == Cell::None {
			return;
		}
		let color = match cell {
			Cell::Pellet => Color::GREEN,
			_ => Color::WHITE
		};
		let ent_cmd = commands.spawn((
			SpriteBundle {
				transform: Transform{
					translation: Vec3::new(coordinate.0 as f32, coordinate.1 as f32, -1.0),
					scale: Vec3::new(0.0, 0.0, 1.0),
					..Default::default()
				},
				sprite: Sprite{
					color: color,
					..Default::default()
				},
				..Default::default()
			},
			effects::GrowIn{
				target_size: Vec2::ONE,
				speed: 5.0
			}
		));
		self.entities.insert(coordinate, ent_cmd.id());
	}
}

// Systems: ----------------------------------------------------------------------------------------

fn initialize(mut commands: Commands){
	commands.spawn(
		GridVisualizer {
			entities: HashMap::new()
		}
	);
}

fn handle_grid_visuals(
	mut commands: Commands, 
	mut grid: ResMut<Grid>, 
	mut query: Query<&mut GridVisualizer>,
	entities: Query<Entity>
) {
	for mut grid_visualizer in &mut query {
		for key in &grid.modified {
			let cell = grid.get_cell_at(key.into());
			if cell == Cell::None {
				let entity = grid_visualizer.entities.get(key);
				if let Some(entity) = entity {
					if entities.contains(*entity) {
						commands.entity(*entity).despawn();
						grid_visualizer.entities.remove(key);
					}
				}
			}
			else {
				let entity = grid_visualizer.entities.get(key);
				if let Some(entity) = entity {
					if entities.contains(*entity) {
						commands.entity(*entity).despawn();
						grid_visualizer.entities.remove(key);
					}
				}
				grid_visualizer.add_cell(&mut commands, *key, cell);
			}
		}
		grid.modified.clear();
	}
}

fn handle_pellet_spawning(
	mut grid: ResMut<Grid>,
	mut pellet_spawner: ResMut<PelletSpawner>,
	crawler_query: Query<&crawler::Crawler>
) {
	if pellet_spawner.pellet_count >= MAX_PELLETS {
		return;
	}

	let crawler = crawler_query.get_single();
	if let Ok(crawler) = crawler {
		if spawn_random_pellet(crawler.grid_position, &mut grid) {
			pellet_spawner.pellet_count += 1;
		}
	}
}

// Utility: ----------------------------------------------------------------------------------------

// resets and starts the grid related functionality
pub fn reset_start(
	commands: &mut Commands,
	grid: &mut ResMut<Grid>, 
	pellet_spawner: &mut ResMut<PelletSpawner>, 
	visualizer: &mut GridVisualizer,
	entities: &Query<Entity>
) {

	// clear all visual grid entities
	for kv in visualizer.entities.iter() {
		if entities.get(*kv.1).is_ok() {
			let mut ent_cmd = commands.entity(*kv.1);
			ent_cmd.despawn();
		}
	}

	// clear grid data and spawn room
	grid.data.clear();
	grid.modified.clear();
	spawn_room(grid);

	// reset pellets
	pellet_spawner.pellet_count = 0;
}

fn spawn_room(grid: &mut ResMut<Grid>){

	for i in 0 ..= ROOM_SIZE {

		// right wall
		grid.set_cell_at((ROOM_SIZE, i).into(), Cell::Wall);
		grid.set_cell_at((ROOM_SIZE, -i).into(), Cell::Wall);

		// left wall
		grid.set_cell_at((-ROOM_SIZE, i).into(), Cell::Wall);
		grid.set_cell_at((-ROOM_SIZE, -i).into(), Cell::Wall);

		// top
		grid.set_cell_at((-i, ROOM_SIZE).into(), Cell::Wall);
		grid.set_cell_at((i, ROOM_SIZE).into(), Cell::Wall);

		// bottom
		grid.set_cell_at((-i, -ROOM_SIZE).into(), Cell::Wall);
		grid.set_cell_at((i, -ROOM_SIZE).into(), Cell::Wall);
	}
}

// spawns a random pellet that is possible for the crawler to get to, but not too easily
fn spawn_random_pellet(crawler_pos: Coordinate, grid: &mut ResMut<Grid>) -> bool{
	
	let random_pos = get_random_pellet_pos(crawler_pos, &grid);
	if random_pos.is_none() {
		return false;
	}
	let random_pos = random_pos.unwrap();

	let cell_at_pos = grid.get_cell_at(random_pos);
	if cell_at_pos.is_solid() || cell_at_pos == Cell::Pellet {
		return false;
	}

	grid.set_cell_at(random_pos.into(), Cell::Pellet);

	true
}

fn get_random_pellet_pos(crawler_pos: Coordinate, grid: &ResMut<Grid>) -> Option<Coordinate> {

	// choose random position not near crawler and not inside wall
	let mut random_pos = (
		rand::thread_rng().gen_range(-ROOM_SIZE + 1 ..= ROOM_SIZE - 1), 
		rand::thread_rng().gen_range(-ROOM_SIZE + 1 ..= ROOM_SIZE - 1)
	);
	let mut crawler_dif = crawler_pos - random_pos.into();
	while 
		crawler_dif.x.abs() <= 1 || 
		crawler_dif.y.abs() <= 1 || 
		grid.get_cell_at(random_pos.into()).is_solid() 
	{
		random_pos = (
			rand::thread_rng().gen_range(-ROOM_SIZE + 1 ..= ROOM_SIZE - 1), 
			rand::thread_rng().gen_range(-ROOM_SIZE + 1 ..= ROOM_SIZE - 1)
		);
		crawler_dif = crawler_pos - random_pos.into();
	}

	// if diagonal next to wall, invalid
	if 
		grid.get_cell_at(Coordinate::from(random_pos) + (1, 1).into()).is_solid() ||
		grid.get_cell_at(Coordinate::from(random_pos) + (1, -1).into()).is_solid() ||
		grid.get_cell_at(Coordinate::from(random_pos) + (-1, 1).into()).is_solid() ||
		grid.get_cell_at(Coordinate::from(random_pos) + (-1, -1).into()).is_solid()
	{
		return None;
	}

	// if on the same row/column as player, only validate if there's a wall between them
	if crawler_dif.x == 0 || crawler_dif.y == 0 {
		let mut coord_dir = crawler_pos - random_pos.into();
		let mut coord_walk: Coordinate = random_pos.clone().into();
		let dist = coord_dir.x.abs().max(coord_dir.y.abs());
		coord_dir.x = coord_dir.x.signum();
		coord_dir.y = coord_dir.x.signum();
		for _ in 0 .. (dist as u8) {
			if grid.get_cell_at(coord_dir).is_solid() {
				break;
			}
			else if crawler_pos.x == coord_walk.x && crawler_pos.y == coord_walk.y {
				return  None;
			}
			coord_walk += coord_dir;
		}
	}

	// TODO flood fill to see if player can reach it

	Some(random_pos.into())
}