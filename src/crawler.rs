use std::f32::consts::PI;

use bevy::prelude::*;
use crate::{grid::{self, Coordinate, PelletSpawner}, effects::{self, Particles}};

// Constant Fields: --------------------------------------------------------------------------------

const CRAWL_SPEED: f32 = 50.0;
const CRAWLER_COLOR: Color = Color::rgb(0.4, 0.5, 1.0);
const CRAWLER_DRAW_ORDER: f32 = 1.0;
const POINTS_PLACEMENT: u32 = 5;
const POINTS_PELLET: u32 = 100;

// Type Definitions: -------------------------------------------------------------------------------

// score container
#[derive(Resource)]
pub struct Score {
	pub placements: u16,
	pub pellets: u16,
}

// direction for simpler coordinate enumeration
#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
	None,
	Right,
	Up,
	Left,
	Down
}

// crawler is the creature that the player controls
#[derive(Component)]
pub struct Crawler{
	pub grid_position: grid::Coordinate,
	pub move_dir: Direction,
	pub total_moves: u32,
	move_elapsed: f32,
	collected_pellet: bool
}

// the plugin that registers the crawler-related systems
pub struct CrawlerPlugin;

// allows player input to control the crawler
#[derive(Component)]
pub struct Controller;

// Type Implementations: ---------------------------------------------------------------------------

// implement cast from direction to coordinate
impl From<Direction> for grid::Coordinate {
	fn from(value: Direction) -> Self {
		match value {
			Direction::Right => (1,0).into(),
			Direction::Up => (0,1).into(),
			Direction::Left => (-1,0).into(),
			Direction::Down => (0,-1).into(),
			_ => (0, 0).into()
		}
	}
}

impl From<Direction> for f32 {
	fn from(value: Direction) -> Self {
		match value {
			Direction::Right => 0.0,
			Direction::Up => PI * 0.5,
			Direction::Left => PI,
			Direction::Down => PI * -0.5,
			_ => 0.0
		}
	}
}

// cast from direction to Vec3
impl From<&Direction> for Vec3 {
	fn from(value: &Direction) -> Self {
		match value {
			Direction::Right => (1.0, 0.0, 0.0).into(),
			Direction::Up => (0.0, 1.0, 0.0).into(),
			Direction::Left => (-1.0, 0.0, 0.0).into(),
			Direction::Down => (0.0, -1.0, 0.0).into(),
			_ => (0.0, 0.0, 0.0).into()
		}
	}
}

// utility operations for direction enum
impl Direction {
	pub fn to_string(&self) -> &str {
		match self {
			Direction::Right => "Right",
			Direction::Up => "Up",
			Direction::Left => "Left",
			Direction::Down => "Down",
			_ => "None",
		}
	}
	pub fn rotate_cw(&self) -> Direction {
		match self {
			Direction::Right => Direction::Down,
			Direction::Up => Direction::Right,
			Direction::Left => Direction::Up,
			Direction::Down => Direction::Left,
			_ => Direction::None
		}
	}
	pub fn rotate_ccw(&self) -> Direction {
		match self {
			Direction::Right => Direction::Up,
			Direction::Up => Direction::Left,
			Direction::Left => Direction::Down,
			Direction::Down => Direction::Right,
			_ => Direction::None
		}
	}
	pub fn opposite(&self) -> Direction {
		match self {
			Direction::Right => Direction::Left,
			Direction::Up => Direction::Down,
			Direction::Left => Direction::Right,
			Direction::Down => Direction::Up,
			_ => Direction::None
		}
	}
}

impl Crawler {

	// snap crawler to the current grid position
	pub fn snap_to_grid(&mut self, transform: &mut Transform){
		self.move_elapsed = 0.0;
		let grid_position: Vec3 = self.grid_position.into();
		transform.translation.x = grid_position.x;
		transform.translation.y = grid_position.y;
	}

	// move in the specified direction and leave behind a solid terrain
	pub fn move_to_dir(
		&mut self, 
		grid: &mut ResMut<grid::Grid>, 
		dir: &Direction, 
		score: &mut ResMut<Score>
	) {
		// do nothing if can't move in specified direction
		if self.move_dir != Direction::None || dir == &Direction::None {
			return;
		}

		// prevent player from moving through walls
		let dir_coord: Coordinate = (*dir).into();
		if grid.get_cell_at(self.grid_position + dir_coord).is_solid() {
			return;
		}

		// place grid cell at current pos before moving
		if !self.collected_pellet {
			grid.set_cell_at(self.grid_position, grid::Cell::Wall);
			score.placements += 1;
		}
		
		// apply movement
		self.move_dir = *dir;
		self.total_moves += 1;
		self.collected_pellet = false;
	}
}

impl Score {
	pub fn get_total(&self) -> u32{
		(self.placements as u32) * POINTS_PLACEMENT + 
		(self.pellets as u32) * POINTS_PELLET
	}
}

// add systems for crawler related handling
impl Plugin for CrawlerPlugin {
	fn build(&self, app: &mut App) {
		app
			.insert_resource(Score{
				pellets: 0,
				placements: 0
			})
			.add_systems(Update, handle_controller)
			.add_systems(Update, handle_crawler_movement)
		;
	}
}

// Systems: ----------------------------------------------------------------------------------------

pub fn handle_crawler_movement(
	time: Res<Time>, 
	mut grid: ResMut<grid::Grid>, 
	mut score: ResMut<Score>,
	mut pellet_spawner: ResMut<PelletSpawner>,
	mut particles: ResMut<Particles>,
	mut query: Query<(&mut Transform, &mut Crawler)>
) {
	let dt = time.delta_seconds();
	for (mut transform, mut crawler) in &mut query {

		// calculate and apply movement translation
		crawler.move_elapsed += CRAWL_SPEED * dt;
		let move_dir: Vec3 = (&crawler.move_dir).into();
		let movement_delta: Vec3 = move_dir * crawler.move_elapsed;
		let grid_pos: Vec3 = crawler.grid_position.into();
		transform.translation = grid_pos + movement_delta;

		// if move for one grid square is complete
		while crawler.move_elapsed >= 1.0 {
			let move_dir = crawler.move_dir;
			crawler.grid_position += move_dir.into();
			crawler.move_elapsed -= 1.0;

			let dir = crawler.move_dir.opposite();
			let off: Coordinate = crawler.move_dir.into();
			let off: Vec2 = off.into();
			let off = off * 0.5;
			let grid_pos: Vec2 = crawler.grid_position.into();

			// emit trail when moving
			if crawler.move_dir != Direction::None {
				effects::particle_narrow_burst(
					&mut particles,
					CRAWLER_COLOR,
					grid_pos + off,
					1,
					14.0,
					dir.into()
				);
			}

			// collect pellet
			let cur_cell = grid.get_cell_at(crawler.grid_position);
			if cur_cell == grid::Cell::Pellet {
				crawler.move_dir = Direction::None;
				crawler.snap_to_grid(&mut transform);
				crawler.collected_pellet = true;
				grid.set_cell_at(crawler.grid_position, grid::Cell::None);
				pellet_spawner.pellet_count -= 1;
				score.pellets += 1;
				effects::particle_burst(
					&mut particles,
					Color::GREEN,
					crawler.grid_position.into(),
					10,
					40.0
				);
			}

			// check for collision
			let next_cell = grid.get_cell_at(crawler.grid_position + crawler.move_dir.into());
			if next_cell.is_solid() {
				crawler.move_dir = Direction::None;
				crawler.snap_to_grid(&mut transform);
				effects::particle_half_burst(
					&mut particles,
					CRAWLER_COLOR,
					grid_pos + off,
					5,
					30.0,
					dir.into()
				);
			}
		}
	}
}

fn handle_controller(
	input: Res<Input<KeyCode>>, 
	mut grid: ResMut<grid::Grid>, 
	mut score: ResMut<Score>,
	mut query: Query<&mut Crawler, With<Controller>>
) {

	let control_right = input.just_pressed(KeyCode::Right);
	let control_up = input.just_pressed(KeyCode::Up);
	let control_left = input.just_pressed(KeyCode::Left);
	let control_down = input.just_pressed(KeyCode::Down);

	for mut crawler in &mut query {
		if crawler.move_dir == Direction::None {

			// set direction based on key input
			let mut dir = Direction::None;
			if control_right {
				dir = Direction::Right;
			}
			if control_up {
				dir = Direction::Up;
			}
			if control_left {
				dir = Direction::Left;
			}
			if control_down {
				dir = Direction::Down;
			}

			// apply move direction
			crawler.move_to_dir(&mut grid, &dir, &mut score);
		}
	}
}

// Utility: ----------------------------------------------------------------------------------------

pub fn reset_start(
	commands: &mut Commands, 
	score: &mut ResMut<Score>,
	crawler_tup: Option<(&mut Crawler, &mut Transform)>
) {
	// reset score
	score.pellets = 0;
	score.placements = 0;

	// reset crawler
	if crawler_tup.is_none(){
		spawn_crawler(commands);
	}
	else {
		let crawler_tup = crawler_tup.unwrap();
		crawler_tup.0.grid_position.x = 0;
		crawler_tup.0.grid_position.y = 0;
		crawler_tup.0.move_dir = Direction::None;
		crawler_tup.0.snap_to_grid(crawler_tup.1);
	}
}

fn spawn_crawler(commands: &mut Commands){
	commands.spawn((
		SpriteBundle { 
			transform: Transform {
				translation: Vec3::new(0.0, 0.0, CRAWLER_DRAW_ORDER),
				..Default::default()
			},
			sprite: Sprite{
				color: CRAWLER_COLOR,
				..Default::default()
			},
			..Default::default() 
		},
		Crawler {
			grid_position: (0, 0).into(),
			move_dir: Direction::None,
			total_moves: 0,
			move_elapsed: 0.0,
			collected_pellet: false
		},
		Controller
	));
}