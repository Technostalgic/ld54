use bevy::prelude::*;
use crate::crawler;

pub struct UiPlugin;

#[derive(Component)]
pub struct StartScreen;

#[derive(Component)]
pub struct ScoreText;

impl Plugin for UiPlugin {
	fn build(&self, app: &mut App) {
		app
			.add_systems(Startup, initialize)
			.add_systems(Update, handle_score_text)
		;
	}
}

fn initialize(mut commands: Commands){

	let mut sections_title: Vec<TextSection> = Vec::new();
	sections_title.push(TextSection { 
		value: "PELLET\nBOI".to_string(),
		style: TextStyle { 
			font_size: 100.0,
			color: Color::WHITE,
			..Default::default() 
		}
	});
	
	let mut sections_start_prompt: Vec<TextSection> = Vec::new();
	sections_start_prompt.push(TextSection { 
		value: "PRESS ENTER TO START".to_string(), 
		style: TextStyle { 
			font_size: 20.0,
			color: Color::WHITE,
			..Default::default() 
		}
	});

	// create title screen
	commands.spawn((
		Text2dBundle{
			transform: Transform {
				translation: Vec3 { x: 0.0, y: 5.0, z: 0.0 },
				scale: Vec3::new(1.0 / crate::CAMERA_ZOOM, 1.0 / crate::CAMERA_ZOOM, 1.0),
				..Default::default()
			},
			text: Text{
				sections: sections_title,
				alignment: TextAlignment::Center,
				..Default::default()
			},
			..Default::default()
		}, StartScreen
	));
	
	// create start prompt
	commands.spawn((
		Text2dBundle{
			transform: Transform {
				translation: Vec3 { x: 0.0, y: -5.0, z: 0.0 },
				scale: Vec3::new(1.0 / crate::CAMERA_ZOOM, 1.0 / crate::CAMERA_ZOOM, 1.0),
				..Default::default()
			},
			text: Text{
				sections: sections_start_prompt,
				..Default::default()
			},
			..Default::default()
		}, StartScreen
	));
}

fn handle_score_text(score: Res<crawler::Score>, mut query: Query<&mut Text, With<ScoreText>>){
	for mut score_text in &mut query {
		score_text.sections[1].value = score.get_total().to_string();
	}
}

pub fn create_game_ui(commands: &mut Commands){
	
	let mut sections_score: Vec<TextSection> = Vec::new();
	sections_score.push(TextSection { 
		value: "SCORE: ".to_string(),
		style: TextStyle { 
			font_size: 15.0,
			color: Color::YELLOW,
			..Default::default() 
		}
	});
	sections_score.push(TextSection { 
		value: "0".to_string(),
		style: TextStyle { 
			font_size: 20.0,
			color: Color::YELLOW,
			..Default::default() 
		}
	});
	
	let mut sections_restart_prompt: Vec<TextSection> = Vec::new();
	sections_restart_prompt.push(TextSection { 
		value: "PRESS ENTER TO RESET".to_string(),
		style: TextStyle { 
			font_size: 15.0,
			color: Color::GRAY,
			..Default::default() 
		}
	});
	
	// create score text
	commands.spawn((
		Text2dBundle{
			transform: Transform {
				translation: Vec3 { x: 0.0, y: 8.0, z: 0.0 },
				scale: Vec3::new(1.0 / crate::CAMERA_ZOOM, 1.0 / crate::CAMERA_ZOOM, 1.0),
				..Default::default()
			},
			text: Text{
				sections: sections_score,
				..Default::default()
			},
			..Default::default()
		}, ScoreText
	));
	
	// create reset prompt text
	commands.spawn(
		Text2dBundle{
			transform: Transform {
				translation: Vec3 { x: 0.0, y: -8.0, z: 0.0 },
				scale: Vec3::new(1.0 / crate::CAMERA_ZOOM, 1.0 / crate::CAMERA_ZOOM, 1.0),
				..Default::default()
			},
			text: Text{
				sections: sections_restart_prompt,
				..Default::default()
			},
			..Default::default()
		}
	);
}