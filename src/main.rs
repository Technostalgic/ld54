use bevy::{prelude::*, window::WindowResolution};
use grid::{Grid, PelletSpawner};

mod effects;
mod grid;
mod crawler;
mod interface;

const CAMERA_ZOOM: f32 = 30.0;
const WINDOW_WIDTH: u16 = 512;
const WINDOW_HEIGHT: u16 = 512;

fn main() {
    let mut app = App::new();
    app
        .add_plugins((
            DefaultPlugins,
            grid::GridPlugin,
            crawler::CrawlerPlugin,
            effects::EffectsPlugin,
            interface::UiPlugin
        ))
        .add_systems(Startup, initialize)
        .add_systems(Update, handle_master_input)
        .run()
    ;
}

fn initialize(mut commands: Commands, mut windows: Query<&mut Window>) {

    let bg_color = Color::rgb(0.1, 0.1, 0.1);

    let mut window = windows.single_mut();
    window.resolution = WindowResolution::new(WINDOW_WIDTH as f32, WINDOW_HEIGHT as f32);

    // create camera
    commands.spawn(
        Camera2dBundle{ 
            transform: Transform { 
                scale: Vec3::new(1.0 / CAMERA_ZOOM, 1.0 / CAMERA_ZOOM, 1.0),
                ..Default::default()
            },
            camera_2d: Camera2d {
                clear_color: bevy::core_pipeline::clear_color::ClearColorConfig::Custom(bg_color)
            },
            ..Default::default() 
        }
    );
}

fn handle_master_input(
    mut commands: Commands, 
    input: Res<Input<KeyCode>>,
    mut grid: ResMut<Grid>,
    mut score: ResMut<crawler::Score>,
    mut pellet_spawner: ResMut<PelletSpawner>,
    mut grid_visualizer_query: Query<&mut grid::GridVisualizer>,
    mut crawler_query: Query<(&mut crawler::Crawler, &mut Transform)>,
    entities: Query<Entity>,
    mut title_screen_query: Query<Entity, With<interface::StartScreen>>
) {
    if input.just_pressed(KeyCode::Return) {
        let mut grid_visualizer = grid_visualizer_query.single_mut();
        match crawler_query.get_single_mut() {
            Ok(mut crawler_tup) => reset_start(
                &mut commands, 
                &mut grid,
                &mut score,
                &mut pellet_spawner,
                &mut grid_visualizer,
                &entities,
                Some((&mut crawler_tup.0, &mut crawler_tup.1))
            ),
            _ => reset_start(
                &mut commands,
                &mut grid,
                &mut score,
                &mut pellet_spawner,
                &mut grid_visualizer,
                &entities,
                None
            )
        }
        let mut build_game_ui = false;
        for title_screen_element in &mut title_screen_query {
            commands.entity(title_screen_element).despawn();
            build_game_ui = true;
        }
        if build_game_ui {
            interface::create_game_ui(&mut commands);
        }
    }
}

fn reset_start(
    mut commands: &mut Commands, 
    grid: &mut ResMut<grid::Grid>,
    score: &mut ResMut<crawler::Score>,
    pellet_spawner: &mut ResMut<PelletSpawner>,
    grid_visualizer: &mut grid::GridVisualizer,
    entities: &Query<Entity>,
    crawler_tup: Option<(&mut crawler::Crawler, &mut Transform)>
){
    grid::reset_start(&mut commands, grid, pellet_spawner, grid_visualizer, entities);
    crawler::reset_start(&mut commands, score, crawler_tup);
}